module de.rezepte.projekt {
    requires transitive javafx.controls;
    requires transitive javafx.fxml;
    requires transitive javafx.graphics;

    exports de.rezepte.projekt;
    exports de.rezepte.projekt.util;
    exports de.rezepte.projekt.models;
    exports de.rezepte.projekt.controller;
    exports de.rezepte.projekt.controller.startpage;

    opens de.rezepte.projekt to javafx.fxml,javafx.graphics;
    opens de.rezepte.projekt.controller to javafx.fxml,javafx.graphics;
    opens de.rezepte.projekt.util to javafx.fxml,javafx.graphics;
    opens de.rezepte.projekt.models to javafx.fxml,javafx.graphics;
    opens de.rezepte.projekt.controller.startpage to javafx.fxml, javafx.graphics;
}