package de.rezepte.projekt.controller.startpage;

import de.rezepte.projekt.Starter;
import de.rezepte.projekt.controller.DetailsController;
import de.rezepte.projekt.controller.ReceiptBaseController;
import de.rezepte.projekt.util.IViewPopulator;
import javafx.fxml.FXML;

public class ReceiptController extends ReceiptBaseController implements IViewPopulator {

	public static String getViewName() {
        return "partials/receipt";
    }

	@FXML
	public void openReceipt() {
		DetailsController detailPage = Starter.sceneHandler.<DetailsController>switchView(DetailsController.getViewName());
		detailPage.modelObj = modelObj;
		detailPage.populateView();
	}
	
}
