package de.rezepte.projekt.controller.startpage;

import de.rezepte.projekt.models.FilterOption;
import de.rezepte.projekt.util.IViewPopulator;
import de.rezepte.projekt.util.SceneHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.List;

public class FilterController implements IViewPopulator {
    @FXML
    public VBox RootBox;
    @FXML
    public Text ColumnTitle;

    public ArrayList<CheckBox> filterChecks = new ArrayList<CheckBox>();

    public List<? extends FilterOption> options;
    public ArrayList<VBox> receipts;

    @Override
    public void populateView() {
        for(FilterOption optionModel : options) {
            HBox option = SceneHandler.<HBox, Object>loadView("partials/filterOption").first();
            ArrayList<Node> children = SceneHandler.getAllChildren(option);
            //TODO: register event listeners
            children.stream().filter(e -> e instanceof CheckBox).map(e -> (CheckBox)e).findFirst().get().setText(optionModel.name);

            RootBox.getChildren().add(option);
            //TODO: use grid pane align icons right and checkboxes left
            //TODO: increase padding so that line is not directly above icons
            //TODO: use flow pane or tile pane or scroll pane to avoid overlapping of area
        }
    }

    public static String getViewName() {
        return "partials/filterList";
    }
}
