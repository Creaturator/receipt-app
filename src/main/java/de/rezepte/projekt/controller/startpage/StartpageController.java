package de.rezepte.projekt.controller.startpage;

import de.rezepte.projekt.Starter;
import de.rezepte.projekt.controller.CreateController;
import de.rezepte.projekt.controller.DetailsController;
import de.rezepte.projekt.models.FilterOption;
import de.rezepte.projekt.models.Receipt;
import de.rezepte.projekt.util.SceneHandler;
import de.rezepte.projekt.util.Tuple;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;

public class StartpageController implements Initializable {
	
	@FXML
    public TextField SearchBar;
    @FXML
    public ImageView SettingsButton;
    @FXML
    public MenuItem FilterEntry;
    @FXML
    public VBox ReceiptsWrapper;
    @FXML
    public TilePane FavoriteReceipts;
    @FXML
    public TilePane RemainderReceipts;
    @FXML
    public Text FavoriteMissingText;
    @FXML
    public StackPane RandomButton;


    public HBox advancedFilterBox = null;
    private ArrayList<Receipt> receipts;

    @Override
	public void initialize(URL location, ResourceBundle resources) {
        prepareSearchField();
        loadReceipts();
	}

    private void prepareSearchField() {
        SearchBar.focusedProperty().addListener((f) -> toggleRandomButton());
    }

    private void loadReceipts() {
        //TODO: Replace dummy receipts with real ones
        receipts = Starter.dummyReceipts();
        for(Receipt modelData : receipts) {
            Tuple<VBox, ReceiptController> receiptTuple = SceneHandler.<VBox, ReceiptController>loadView(ReceiptController.getViewName());
            receiptTuple.second().modelObj = modelData;
            receiptTuple.second().populateView();
            (modelData.isFavorite ? FavoriteReceipts : RemainderReceipts).getChildren().add(0, receiptTuple.first());
            TilePane.setMargin(receiptTuple.first(), new Insets(10));
            for(ImageView star : SceneHandler.getAllChildren(receiptTuple.first()).stream().filter(e -> e.getId() != null && e.getId().contains("Star")).map(e -> (ImageView)e).toList())
                star.addEventFilter(MouseEvent.MOUSE_CLICKED, (e) -> updateFavorites((ImageView)e.getPickResult().getIntersectedNode()));
        }
        FavoriteMissingText.setVisible(FavoriteReceipts.getChildren().isEmpty());
    }

    public static String getViewName() {
        return "startpage";
    }

    private void toggleRandomButton() {
        RandomButton.setVisible(!SearchBar.isFocused() && advancedFilterBox != null);
    }

    private void updateFavorites(ImageView clickedStar) {
        clickedStar.requestFocus();
        Parent parentNode = clickedStar.getParent();
        while((parentNode.getId() == null || !parentNode.getId().equals("receiptRoot"))) {
            parentNode = parentNode.getParent();
            if (parentNode == null)
                throw new IllegalStateException("No fitting parent found");
        }
        boolean wasFavorite = clickedStar.getId().toLowerCase().contains("filled");
        (wasFavorite ? FavoriteReceipts : RemainderReceipts).getChildren().remove(parentNode);
        (wasFavorite ? RemainderReceipts : FavoriteReceipts).getChildren().add(0, parentNode);

        FavoriteMissingText.setVisible(FavoriteReceipts.getChildren().isEmpty());
        clickedStar.requestFocus();
    }

    public void showRandomReceipt() {
        DetailsController detailsPage = Starter.sceneHandler.<DetailsController>switchView(DetailsController.getViewName());
    	detailsPage.modelObj = receipts.get(new Random().nextInt(receipts.size()));
        detailsPage.populateView();
    }

    public void openAdvancedSearch() {
        SettingsButton.requestFocus();
        toggleRandomButton();
        if (advancedFilterBox == null) {
            //TODO: replace with flow pane or tile pane or scroll bar
            advancedFilterBox = new HBox();
            ArrayList<VBox> receiptTemplates = new ArrayList<VBox>(FavoriteReceipts.getChildren().filtered(e -> e instanceof VBox).stream().map(e -> (VBox)e).toList());
            receiptTemplates.addAll(RemainderReceipts.getChildren().filtered(e -> e instanceof VBox).stream().map(e -> (VBox)e).toList());
            prepareFilterColumn("Ingredients", Starter.dummyIngredients().stream().limit(6).toList(), receiptTemplates);
            prepareFilterColumn("", Starter.dummyIngredients().stream().skip(6).toList(), receiptTemplates);
            prepareFilterColumn("Diets", Starter.dummyDiets(), receiptTemplates);
            ReceiptsWrapper.getChildren().add(0, advancedFilterBox);
            advancedFilterBox.setAlignment(Pos.TOP_CENTER);
            FilterEntry.setText("Close filters");
        } else {
            //TODO: Potential memory leak because of children not being cleared but still referenced by Filter box
            ReceiptsWrapper.getChildren().remove(advancedFilterBox);
            advancedFilterBox = null;
            FilterEntry.setText("Filter receipts");
        }
    }
    private void prepareFilterColumn(String columnTitle, List<? extends FilterOption> options, ArrayList<VBox> receiptTemplates) {
        Tuple<VBox, FilterController> filterListTuple = SceneHandler.<VBox, FilterController>loadView(FilterController.getViewName());
        filterListTuple.second().options = options;
        filterListTuple.second().receipts = receiptTemplates;
        filterListTuple.second().populateView();
        filterListTuple.second().ColumnTitle.setText(columnTitle);
        advancedFilterBox.getChildren().add(filterListTuple.first());
    }

    @FXML
    public void addIngredient() {
    }

    @FXML
    public void addReceipt() {
        Starter.sceneHandler.switchView(CreateController.getViewName());
    }

    @FXML
    public void addDiet() {
    }
}
