package de.rezepte.projekt.controller;

import de.rezepte.projekt.models.Receipt;
import de.rezepte.projekt.util.IViewPopulator;
import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

public abstract class ReceiptBaseController implements IViewPopulator {

    @FXML
    public ImageView Star;
    @FXML
    public ImageView StarFilled;
    @FXML
    public ImageView Image;
    @FXML
    public Text Name;

    public Receipt modelObj;

    @Override
    public void populateView() {
        StarFilled.setVisible(modelObj.isFavorite);
        Star.setVisible(!StarFilled.isVisible());
        Image.setImage(modelObj.image);
        Name.setText(modelObj.name);
    }

    @FXML
    public void toggleFavorite() {
        Star.setVisible(!Star.isVisible());
        StarFilled.setVisible(!StarFilled.isVisible());

        modelObj.isFavorite = StarFilled.isVisible();
    }
}
