package de.rezepte.projekt.controller;

import de.rezepte.projekt.models.FilterOption;
import de.rezepte.projekt.util.IViewPopulator;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

@SuppressWarnings("unchecked")
public class TableController implements IViewPopulator {

    @FXML
    public TableView<FilterOption> IngredientTable;

    public ObservableList<? extends FilterOption> modelObjs;

    public static String getViewName() {
        return "partials/table";
    }

    @Override
    public void populateView() {
        IngredientTable.setItems((ObservableList<FilterOption>) modelObjs);
        TableColumn<FilterOption, String> column = (TableColumn<FilterOption, String>) IngredientTable.getColumns().<FilterOption, String>get(0);
        column.setCellValueFactory((e) -> e.getValue().getNameAsProperty());
        int tableHeight = (int)Math.round(IngredientTable.getFixedCellSize() * (modelObjs.size() + 1));
        IngredientTable.setMinHeight(tableHeight);
        IngredientTable.setMaxHeight(tableHeight);
        IngredientTable.setPrefHeight(tableHeight);
    }
}
