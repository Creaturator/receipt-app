package de.rezepte.projekt.controller;

import de.rezepte.projekt.Starter;
import de.rezepte.projekt.controller.startpage.StartpageController;
import de.rezepte.projekt.util.IViewPopulator;
import de.rezepte.projekt.util.SceneHandler;
import de.rezepte.projekt.util.Timer;
import de.rezepte.projekt.util.Tuple;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

import java.util.Arrays;

public class DetailsController extends ReceiptBaseController implements IViewPopulator {

    @FXML
    public StackPane TablePane;
    @FXML
    public Button CopyButton;
    @FXML
    public Text PreparationText;

    public static String getViewName() {
        return "details";
    }

    @Override
    public void populateView() {
        super.populateView();
        fillTable();
        PreparationText.setText(modelObj.description);
    }

    private void fillTable() {
        Tuple<Group, TableController> tableTuple = SceneHandler.<Group, TableController>loadView(TableController.getViewName());
        TablePane.getChildren().add(tableTuple.first());
        tableTuple.second().modelObjs = modelObj.getIngredientsAsObservable();
        tableTuple.second().populateView();
    }

    @FXML
    public void copyToClipboard() {
        CopyButton.requestFocus();

        Clipboard clipboard = Clipboard.getSystemClipboard();
        ClipboardContent content = new ClipboardContent();
        content.putString(Arrays.stream(modelObj.ingredients).skip(1).map(i -> i.name).reduce(modelObj.ingredients[0].name, (c, i) -> c + ", " + i));
        clipboard.setContent(content);

        CopyButton.setText(clipboard.hasString() ? "Copied" : "Failed to copy");
        Timer.startTimer(2f, () -> CopyButton.setText("Copy ingredients"));
    }

    @FXML
    public void deleteReceipt() {
        //TODO: Delete receipt
        Starter.sceneHandler.switchView(StartpageController.getViewName());
    }

    @FXML
    public void editReceipt() {
        CreateController editPage = Starter.sceneHandler.switchView(CreateController.getViewName());
        editPage.modelObj = modelObj;
        editPage.populateView();
    }

    @FXML
    public void returnToStartpage() {
        //TODO: save receipt to file
        Starter.sceneHandler.<StartpageController>switchView(StartpageController.getViewName());
    }
}
