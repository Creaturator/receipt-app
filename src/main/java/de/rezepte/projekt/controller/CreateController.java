package de.rezepte.projekt.controller;

import de.rezepte.projekt.Starter;
import de.rezepte.projekt.controller.startpage.StartpageController;
import de.rezepte.projekt.models.Ingredient;
import de.rezepte.projekt.models.Receipt;
import de.rezepte.projekt.util.IViewPopulator;
import de.rezepte.projekt.util.SceneHandler;
import de.rezepte.projekt.util.Timer;
import de.rezepte.projekt.util.Tuple;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;

import java.io.File;

public class CreateController implements IViewPopulator {
    @FXML
    public Text Title;
    @FXML
    public Button ImageButton;
    @FXML
    public VBox IngredientsBox;
    @FXML
    public TextArea PreparationField;
    @FXML
    public TextField NameField;

    public Receipt modelObj;
    private TableView<Ingredient> ingredientTable;

    public static String getViewName() {
        return "create";
    }

    @Override
    public void populateView() {
        Title.setText("Edit " + modelObj.name);
        NameField.setText(modelObj.name);
        PreparationField.setText(modelObj.description);
        ImageButton.setText(modelObj.image.getUrl().substring(modelObj.image.getUrl().indexOf(':') + 2));
        loadTable();
        ingredientTable.getItems().addAll(modelObj.getIngredientsAsObservable());
    }

    private void loadTable() {
        Tuple<Group, TableController> tableTuple = SceneHandler.<Group, TableController>loadView(TableController.getViewName());

        IngredientsBox.getChildren().add(tableTuple.first());
        VBox.setMargin(tableTuple.first(), new Insets(5, 0, 0, 0));
        //noinspection unchecked
        ingredientTable = (TableView<Ingredient>) tableTuple.first().getChildren().get(0);
        TableColumn<Ingredient, ImageView> editRow = new TableColumn<Ingredient, ImageView>("");
        editRow.setCellValueFactory((e) -> loadTableButton("pencil", e.getValue()));
        editRow.setPrefWidth(35);
        editRow.setMaxWidth(35);
        ingredientTable.getColumns().add(editRow);
        TableColumn<Ingredient, ImageView> deleteRow = new TableColumn<Ingredient, ImageView>("");
        deleteRow.setCellValueFactory((e) -> loadTableButton("bin", e.getValue()));
        deleteRow.setPrefWidth(35);
        deleteRow.setMaxWidth(35);
        ingredientTable.getColumns().add(deleteRow);
        ingredientTable.getColumns().get(0).setPrefWidth(530);
        ingredientTable.getColumns().get(0).setMaxWidth(530);

        tableTuple.second().modelObjs = modelObj.getIngredientsAsObservable();
        tableTuple.second().populateView();
    }

    private SimpleObjectProperty<ImageView> loadTableButton(String imageName, Ingredient modelObj) {
        ImageView buttonIcon = new ImageView(SceneHandler.getIconPath(imageName).toString());
        buttonIcon.setFitHeight(20);
        buttonIcon.setFitWidth(20);
        //TODO: add event listeners and include model object
        return new SimpleObjectProperty<ImageView>(buttonIcon);
    }

    @FXML
    public void returnToPrevious() {
        if (modelObj == null)
            Starter.sceneHandler.switchView(StartpageController.getViewName());
        else {
            DetailsController detailsPage = Starter.sceneHandler.switchView(DetailsController.getViewName());
            detailsPage.modelObj = modelObj;
            detailsPage.populateView();
        }
    }

    @FXML
    public void saveReceipt() {
        modelObj.name = NameField.getText();
        //TODO: Ingredients are duplicated at each save
        modelObj.ingredients = ingredientTable.getItems().toArray(Ingredient[]::new);
        modelObj.image = new Image("file:/" + ImageButton.getText());
        modelObj.description = PreparationField.getText();
        returnToPrevious();
    }

    @FXML
    public void addIngredient() {
        //TODO: Popup einfugen
        Ingredient newIngredient = null;
        if (ingredientTable == null) {
            loadTable();
        }
        ingredientTable.getItems().add(newIngredient);
    }

    @FXML
    public void selectImage() {
        try {
            FileChooser fileLoader = new FileChooser();
            fileLoader.setTitle("Choose an image for the new receipt");
            fileLoader.setInitialDirectory(new File("C:\\Users\\" + System.getProperty("user.name") + "\\Pictures"));
            fileLoader.getExtensionFilters().add(new FileChooser.ExtensionFilter("jpeg image", "*.jpg"));
            fileLoader.getExtensionFilters().add(new FileChooser.ExtensionFilter("png image", "*.png"));
            fileLoader.getExtensionFilters().add(new FileChooser.ExtensionFilter("all image formats", "*.jpg", "*.jpeg", "*.png"));
            File image = fileLoader.showOpenDialog(ImageButton.getScene().getWindow());
            if (image == null) {
                ImageButton.setText("Failed to add image");
                Timer.startTimer(2f, () -> ImageButton.setText("Click to add an image"));
            } else
                ImageButton.setText(image.getAbsolutePath());
        } catch(Exception e) {
            e.printStackTrace();
            ImageButton.setText("Failed to add image");
            Timer.startTimer(2f, () -> ImageButton.setText("Click to add an image"));
        }
    }


}
