package de.rezepte.projekt;

import de.rezepte.projekt.controller.startpage.StartpageController;
import de.rezepte.projekt.models.Diet;
import de.rezepte.projekt.models.Ingredient;
import de.rezepte.projekt.models.Receipt;
import de.rezepte.projekt.util.SceneHandler;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.net.URI;
import java.util.ArrayList;

public class Starter extends Application {
	
	public static SceneHandler sceneHandler;
	
    @Override
    public void start(Stage fenster) {
        fenster.setTitle("Digital receipt book");
        sceneHandler = new SceneHandler(fenster);
        sceneHandler.switchView(StartpageController.getViewName());
        fenster.show();
    }

    public static ArrayList<Diet> dummyDiets() {
        ArrayList<Diet> dummyDiets = new ArrayList<Diet>(2);
        dummyDiets.add(new Diet("Vegan"));
        dummyDiets.add(new Diet("Vegetarian"));
        return dummyDiets;
    }

    public static ArrayList<Ingredient> dummyIngredients() {
        ArrayList<Ingredient> dummyIngredients = new ArrayList<Ingredient>(4);
        dummyIngredients.add(new Ingredient("Milch", new Diet[0]));
        dummyIngredients.add(new Ingredient("Hackfleisch", new Diet[0]));
        dummyIngredients.add(new Ingredient("Mohre", new Diet[0]));
        dummyIngredients.add(new Ingredient("Tomatensauce", new Diet[0]));
        dummyIngredients.add(new Ingredient("Schlagsahne", new Diet[0]));
        dummyIngredients.add(new Ingredient("Ajvar", new Diet[0]));
        dummyIngredients.add(new Ingredient("Zwiebel", new Diet[0]));
        dummyIngredients.add(new Ingredient("Ei", new Diet[0]));
        dummyIngredients.add(new Ingredient("Mehl", new Diet[0]));
        return dummyIngredients;
    }

    public static ArrayList<Receipt> dummyReceipts() {
        URI baseURL = null;
        ArrayList<Receipt> dummyReceipts = new ArrayList<Receipt>(3);

        dummyReceipts.add(new Receipt("Bolognese", new Image(SceneHandler.getDummyPicturePath("_20230829_220047").toString(), 195, 200, false, false), new Ingredient[]{new Ingredient("Tomatensauce", new Diet[0]), new Ingredient("Hackfleisch", new Diet[0]), new Ingredient("Mohre", new Diet[0])}, lipsum()));
        dummyReceipts.add(new Receipt("Frikadellen", new Image(SceneHandler.getDummyPicturePath("_20231029_005947").toString(), 195, 200, false, false), new Ingredient[]{new Ingredient("Zwiebel", new Diet[0]), new Ingredient("Ei", new Diet[0])}, lipsum()));
        dummyReceipts.add(new Receipt("Nudel-Kartoffel-Mix mit Sahnesauce", new Image(SceneHandler.getDummyPicturePath("_20231124_194829").toString(), 195, 200, false, false), new Ingredient[]{new Ingredient("Ajvar", new Diet[0]), new Ingredient("Schlagsahne", new Diet[0])}, lipsum()));
        dummyReceipts.add(new Receipt("Pfannekuchen", null, new Ingredient[]{new Ingredient("Mehl", new Diet[0]), new Ingredient("Milch", new Diet[0])}, "grosse Pfannekuchen"));
        return dummyReceipts;
    }

    public static String lipsum() {
        return "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris semper et enim a sollicitudin. Sed id metus dignissim risus viverra dignissim. Donec vitae vulputate quam. Morbi luctus fringilla ex nec vestibulum. Mauris sed justo velit. Praesent eleifend quis metus non imperdiet. Nunc eleifend vitae velit sit amet dignissim. Praesent vel luctus nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae;\n" +
                "\n" +
                "Sed tortor arcu, molestie a pharetra vehicula, molestie eu nisl. Mauris tincidunt ac quam vel tincidunt. Donec condimentum pharetra nulla vitae fermentum. Praesent lacus ipsum, semper sed mattis sed, placerat id magna. Praesent finibus eros erat, at facilisis augue placerat non. In justo turpis, tristique nec lorem iaculis, tincidunt eleifend quam. Donec at nunc et enim mattis maximus et nec felis. Nullam posuere elementum orci vel rhoncus. Cras facilisis posuere est. Curabitur nisl risus, posuere id tempus sit amet, facilisis ut nibh.\n" +
                "\n" +
                "Integer felis nunc, laoreet a tortor id, gravida convallis urna. Praesent quis accumsan ante, sed volutpat ex. Fusce ultrices, purus at pulvinar dictum, orci urna tincidunt nisi, sit amet pharetra enim libero et mi. In neque purus, mattis quis augue sit amet, ornare lobortis justo. Mauris sodales sapien nec nisi commodo, ac euismod ligula suscipit. Aliquam cursus eros dolor, at consectetur lacus fringilla eget. Proin nec feugiat justo. Duis semper sodales libero facilisis euismod. Aliquam non aliquet nibh, lacinia blandit sem. Pellentesque vehicula facilisis euismod. Aliquam quis dolor in ex sollicitudin mollis quis eget sem. Praesent pulvinar laoreet quam nec egestas. Mauris semper volutpat ex, ut lacinia dolor dapibus vel. Aliquam congue diam a tortor lobortis hendrerit. Morbi nec vestibulum elit.";
    }

    public static void main(String[] args) {
        launch(args);
    }
}