package de.rezepte.projekt;

import javafx.application.Application;

public class BuildStarter {

    public static void main(String[] args) {
        Application.launch(Starter.class, args);
    }
}
