package de.rezepte.projekt.util;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.util.Duration;

public class Timer {

    public static void startTimer(float seconds, Runnable onComplete) {
        new Timeline(new KeyFrame(Duration.seconds(seconds), ae -> onComplete.run())).play();
    }
}
