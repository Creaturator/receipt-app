package de.rezepte.projekt.util;

public record Tuple<F, S>(F first, S second) { }
