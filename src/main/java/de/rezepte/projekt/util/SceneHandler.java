package de.rezepte.projekt.util;

import de.rezepte.projekt.Starter;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;

public class SceneHandler {

	private final Stage window;
	
	public SceneHandler(Stage window) {
		this.window = window;
	}
	
	public <C> C switchView(String viewPath) {
		Tuple<Parent, C> viewTuple = SceneHandler.<Parent, C>loadView(viewPath);
        window.setScene(new Scene(viewTuple.first()));
		return viewTuple.second();
    }

	public static <P extends Parent, C> Tuple<P, C> loadView(String viewPath) {
		try {
			URL viewURL = getViewURL(viewPath);
			if (viewURL == null) {
				System.err.println("The view path is null");
				return new Tuple<P, C>(null, null);
			}
			FXMLLoader fileParser = new FXMLLoader(getViewURL(viewPath));
			return new Tuple<P, C>(fileParser.load(), fileParser.getController());
		} catch (IOException e) {
			e.printStackTrace();
			return new Tuple<P, C>(null, null);
		}
	}

	public static URL getViewURL(String viewPath) {
		return Starter.class.getResource("views/" + viewPath + ".fxml");
	}

	public static URL getIconPath(String filename) {
		return Starter.class.getResource("icons/" + filename + ".png");
	}

	public static URL getDummyPicturePath(String filename) {
		return Starter.class.getResource("dummyPictures/" + filename + ".JPG");
	}

	/**
	 * Copyright <a href="https://stackoverflow.com/questions/41932635/scanning-classpath-modulepath-in-runtime-in-java-9">Stackoverflow</a>
	 */
	private static URI getBaseURI() {
		try {
			String path = Objects.requireNonNull(SceneHandler.class.getResource(
					SceneHandler.class.getSimpleName() + ".class")).toURI().toString();
			path = path.substring(0, path.lastIndexOf('/'));
			path = path.substring(0, path.lastIndexOf('/'));
			return URI.create(path);
		} catch (URISyntaxException e) {
			e.printStackTrace();
			return null;
		}
    }

	public static ArrayList<Node> getAllChildren(Parent parentNode) {
		ObservableList<Node> directChilds = parentNode.getChildrenUnmodifiable();
		ArrayList<Node> allChilds = new ArrayList<Node>();
		if (!directChilds.isEmpty()) {
			if (directChilds.filtered((c) -> c instanceof Parent).isEmpty())
				allChilds.addAll(directChilds);
			else {
				for (Node child : directChilds) {
					if (child instanceof Parent) {
						allChilds.addAll(getAllChildren((Parent) child));
						allChilds.add(child);
					}
					else
						allChilds.add(child);
				}
			}
		}
		return allChilds;
	}
	
}
