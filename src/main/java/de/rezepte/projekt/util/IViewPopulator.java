package de.rezepte.projekt.util;

public interface IViewPopulator {

    public void populateView();
}
