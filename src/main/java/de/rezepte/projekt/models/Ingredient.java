package de.rezepte.projekt.models;

public class Ingredient extends FilterOption {

    public Diet[] diets;

    public Ingredient(String name, Diet[] diets) {
        super(name);
        this.name = name;
        this.diets = diets;
    }

    @Override
    public void selectedForFiltering() {

    }
}
