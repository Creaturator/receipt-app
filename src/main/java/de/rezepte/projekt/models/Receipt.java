package de.rezepte.projekt.models;

import de.rezepte.projekt.util.SceneHandler;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;

import java.util.Objects;

public class Receipt {

    public String name;
    public Image image;
    public Ingredient[] ingredients;
    public String description;
    public boolean isFavorite;

    public Receipt(String name, Image image, Ingredient[] ingredients, String description) {
        if (image != null && image.isError()) {
            image.getException().printStackTrace();
            image = null;
        }

        this.name = name;
        this.image = Objects.requireNonNullElseGet(image,
                () -> new Image(SceneHandler.getIconPath("dummy_picture").toString(), 195, 200, false, false));
        this.ingredients = ingredients;
        this.description = description;
        isFavorite = false;
    }

    public ObservableList<Ingredient> getIngredientsAsObservable() {
        return FXCollections.observableArrayList(ingredients);
    }

    public SimpleStringProperty getDescriptionAsProperty() {
        return new SimpleStringProperty(description);
    }

}
