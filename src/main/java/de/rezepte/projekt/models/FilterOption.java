package de.rezepte.projekt.models;

import javafx.beans.property.SimpleStringProperty;

public abstract class FilterOption {

    public String name;

    public FilterOption(String name) {
        this.name = name;
    }

    public SimpleStringProperty getNameAsProperty() {
        return new SimpleStringProperty(name);
    }

    public abstract void selectedForFiltering();
}
